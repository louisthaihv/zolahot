// Ionic zolahot App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'zolahot' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'zolahot.controllers' is found in controllers.js
angular.module('zolahot', ['ionic', 'zolahot.controllers', 'zolahot.services','zolahot.directive', 'ngCordova'])

.run(function($ionicPlatform) {
  
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider) {
    $httpProvider.interceptors.push(function ($q, $rootScope) {
            return {
                'response': function (response) {
                    //Will only be called for HTTP up to 300
                    return response;
                },
                'responseError': function (rejection) {
                    if(rejection.status === 400) {
                        location.reload();
                    }
                    if(rejection.status === 401 && rejection.data.error === "invalid_credentials") {
                        $rootScope.authError = "Email / Password is not matched !!!";
                    }
                    if(rejection.status === 401 && rejection.data.error === "invalid_old_pass") {
                        $rootScope.passError = "Old Password is not valid !!!";
                    }
                    return $q.reject(rejection);
                }
            };
        });

    // Tabs at bottom position config
    $ionicConfigProvider.tabs.position('top');
  
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
//route search
  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })
//route home
    .state('app.news', {
      url: '/news',
      views: {
        'menuContent': {
          templateUrl: 'templates/news.html',
          controller: 'NewsCtrl'
        }
      }
    })
//category route
  .state('app.category', {
    url: '/category',
    views: {
      'menuContent':{
        templateUrl: 'templates/category.html',
        controller: 'CategoryController'
      }
    }
  })
//category post
  .state('app.category_detail', {
    url: '/category_detail/:category_id',
    views: {
      'menuContent':{
        templateUrl: 'templates/category_detail.html',
        controller: 'CategoryDetailController'
      }
    }
  })
  .state('app.topnews', {
    url: '/topnews',
    views: {
      'menuContent':{
        templateUrl: 'templates/topnews.html',
        controller: 'TopNewsController'
      }
    }
  })
  .state('app.aboutus', {
    url: '/aboutus',
    views: {
      'menuContent':{
        templateUrl: 'templates/aboutus.html',
        controller: 'AboutUsController'
      }
    }
  })
// detail route
  .state('app.detail', {
    url: '/detail/:post_id',
    views: {
      'menuContent': {
        templateUrl: 'templates/detail.html',
        controller: 'DetailController'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/news');
});


var appCtrl = angular.module('zolahot.controllers', []);
var appServices = angular.module('zolahot.services',[]);
var appDirective = angular.module('zolahot.directive',[]);
