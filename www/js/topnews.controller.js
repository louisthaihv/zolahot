appCtrl.controller('TopNewsController', function ($scope, $ionicSideMenuDelegate, HttpService){
    $scope.init = function() {
        $scope.posts = [];
        $scope.pageNumber = 1;
        $scope.canShowMore = true;
    };
    $scope.getResultsPage = function() {
        var url="";
        if($scope.showSear ==false && $scope.qSearch !==""){
            url = "ajax_search?query=" + $scope.qSearch + "&page=" + $scope.pageNumber;
        }
        else {
            url = 'ajax/post/top?page=' + $scope.pageNumber;
        }
        HttpService.requestAPI('get',url , {}, function(items){
            if (items.length) {
                $scope.pageNumber++;
                $scope.posts = $scope.posts.concat(items);
            }
            else {
                $scope.canShowMore = false;
            }
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });
    };

    $scope.refresh = function() {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.init();
        $scope.getResultsPage();
    };
    
    $scope.search = function (){
        $scope.init();
        $scope.getResultsPage();
    };
    $scope.init();

    $scope.init = function() {
        $scope.posts = [];
        $scope.pageNumber = 1;
        $scope.canShowMore = true;
    };
})


