appCtrl.controller('DetailController', function ($scope, $stateParams, HttpService, $ionicActionSheet, $cordovaSocialSharing, $sce) {

    $scope.init = function() {
        $scope.post = null;
        $scope.canShowMore = true;
    };
    $scope.getResultsPage = function(post_id) {
        if (typeof(post_id)==='undefined'){
          $scope.post_id = $stateParams.post_id;
        }
        else{
          $scope.post_id = post_id;
        }
        var url='ajaxpost/' + $scope.post_id;
        HttpService.requestAPI('get',url , {}, function(items){
            $scope.post = items;
            $scope.post.videoHtml = '<div class="fb-video" data-href="' + items.fb_link + '" data-width="auto" ></div>';
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });
        $scope.canShowMore = false;
    };

    $scope.refresh = function() {
        $scope.$broadcast('scroll.refreshComplete');
        $scope.init();
        $scope.getResultsPage($scope.post_id);
    };
   $scope.init(); 





    $scope.shareAnywhere = function() {
        $cordovaSocialSharing.share("This is your message", "This is your subject", "", "http://zolahot.ml");
    }

    $scope.share = function(id, type){
    $ionicActionSheet.show({
      titleText: 'Select social',
      buttons: [
        { text: '<i class="ion-social-facebook"></i>  Facebook' },
        { text: '<i class="ion-social-twitter"></i>  Tweeter' },
        { text: '<i class="ion-social-googleplus"></i>  Google' },
      ],
      cancelText: 'Cancel',
      cancel: function () {
        console.log('CANCELLED');
      },
      buttonClicked: function(index) {
      	if(index === 0 ){
      		$scope.shareAnywhere();
      	}
      	else if(index === 1 ){
      		alert('tweeter');
      	}
       return true;
     }
    });
    }
})