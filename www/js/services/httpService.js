appServices.service('HttpService', function($http, ConfigService) {
    this.requestAPI = function(method, url, data, next) {
        $http({
            method: method,
            url: ConfigService.baseUrl + url,
            data: data
        }).then(function(response) {
            next(response.data);
        });
    };
});